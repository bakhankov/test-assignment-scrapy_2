# -*- coding: utf-8 -*-
from scrapy import Spider, FormRequest, Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst
from ..items import QuokaItem
from datetime import date


category_url = 'http://www.quoka.de/immobilien/bueros-gewerbeflaechen/'


def get_form_data(comm, page='1'):
    return {'classtype': 'of', 'comm': comm, 'pageno': page}


class QuokaSpider(Spider):
    name = 'Quoka'

    def start_requests(self):
        commercial = ('1', '0')  # '1' for 'Gewerbliche', and '0' for 'Private'.
        return [
            FormRequest(
                category_url,
                formdata=get_form_data(com),
                callback=self.pars_offers_page,
                meta={'comm': com}
            ) for com in commercial
        ]

    def pars_offers_page(self, response):
        offers = response.xpath('//li[@class="q-ln hlisting"]')  # Offers in which we need to open URL.
        for offer in offers:
            url_matches = offer.xpath('.//div[@class="q-col n2"]/a/@href').extract()
            if url_matches:
                yield Request('http://www.quoka.de' + url_matches[0], callback=self.parse_detail_page)
        next_page = response.xpath('//div/ul/li[@class="arr-rgt active"]/a/@data-qng-page').extract()  # Next page value, like "['2']".
        if next_page:
            yield FormRequest(
                category_url,
                formdata=get_form_data(response.meta['comm'], next_page[0]),
                callback=self.pars_offers_page,
                meta={'comm': response.meta['comm']}
            )

    def parse_detail_page(self, response):
        loader = ItemLoader(QuokaItem(), selector=response.xpath('//main'))
        loader.default_output_processor = TakeFirst()
        loader.add_xpath('Uberschrift', './/h1/text()')
        loader.add_xpath('Immobilientyp', '//header//span[@class="category"]/text()')
        loader.add_xpath('Kaufpreis', './/div[@class="price has-type"]/strong/span/text()')
        loader.add_xpath('PLZ', './/div[@class="details"]//span[@class="postal-code"]/text()')
        loader.add_xpath('Stadt', './/div[@class="details"]//span[@class="locality"]/text()')
        loader.add_xpath('OBID', './/div[@class="date-and-clicks"]/strong/text()')
        loader.add_xpath('Erstellungsdatum', './/div[@class="date-and-clicks"]/text()')
        loader.add_xpath('Erstellungsdatum', './/div[@class="date-and-clicks"]/span/text()')
        loader.add_xpath('Beschreibung', './/div[@itemprop="description"]')
        loader.add_xpath('Gewerblich', './/div[@class="cust-type"]/text()')
        loader.add_xpath('Anbieter_ID', './/div[@class="partnerLogo"]//@title')
        loader.add_xpath('Land', './/div[@class="details"]//span[@class="country-name country"]/text()')
        loader.add_value(
            None,
            {
                'Monat': date.today().month,
                'url': response.url,
                'Boersen_ID': '21',
                'erzeugt_am': date.today(),
                'Vermarktungstyp': 'kaufen',
            }
        )
        handy1 = response.xpath('//main//div[@class="meta"]//span/a/@onclick')  # Phone number selector
        if handy1:
            request = Request(
                'http://www.quoka.de/ajax/detail/displayphonenumber.php?' + handy1.re(r"php\?(.*)'")[0],
                callback=self.parse_phone
            )
            request.meta['loader'] = loader
            yield request
        else:
            yield loader.load_item()

    def parse_phone(self, response):
        loader = response.meta['loader']
        loader.add_value('Telefon', response.xpath('//span/text()').extract())
        return loader.load_item()