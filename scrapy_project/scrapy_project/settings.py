# -*- coding: utf-8 -*-
BOT_NAME = 'scrapy_project'

SPIDER_MODULES = ['scrapy_project.spiders']
NEWSPIDER_MODULE = 'scrapy_project.spiders'

ITEM_PIPELINES = {
   'scrapy_project.pipelines.DetailPagePipeline': 1,
}