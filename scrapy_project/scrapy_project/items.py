# -*- coding: utf-8 -*-
from scrapy.item import Item, Field
from w3lib.html import remove_tags, replace_escape_chars
from datetime import date, timedelta, datetime


def only_digit(matches):
    string = matches[0]
    return ''.join([char for char in string if char.isdigit()])


def get_date(matches):
    matches = map(replace_escape_chars, matches)
    matches = map(lambda x: x.rstrip(), matches)
    matches = filter(bool, matches)
    if u'Heute' in matches[0]:
        return date.today()
    elif u'Gestern' in matches[0]:
        return date.today() - timedelta(1)
    elif u'Monaten' in matches[0]:
        return date.today() - timedelta(180)
    else:
        return datetime.strptime(matches[0], "%d.%m.%Y")


def remove_tags_from_list(matches):
    return remove_tags(matches[0])


def choose_comm(matches):
    if u'Privater' in matches[0]:
        return u'0'
    elif u'Gewerblicher' in matches[0]:
        return u'1'


def anbieter_name(matches):
    if matches:
        if [x for x in matches if u'scout24' in x.lower()]:
            return u'Immobilienscout24'
        else:
            return matches[0]


def get_land_name(matches):
    if matches:
        if matches[0] == u'D':
            return u'Deutschland'
        else:
            return matches[0]


class QuokaItem(Item):
    id = Field()
    Boersen_ID = Field()
    OBID = Field(output_processor=only_digit)
    erzeugt_am = Field()
    Anbieter_ID = Field(output_processor=anbieter_name)
    Anbieter_ObjektID = Field()
    Immobilientyp = Field(output_processor=lambda x: x[-1])
    Immobilientyp_detail = Field()
    Vermarktungstyp = Field()
    Land = Field(output_processor=get_land_name)
    Bundesland = Field()
    Bezirk = Field()
    Stadt = Field()
    PLZ = Field()
    Strasse = Field()
    Hausnummer = Field()
    Uberschrift = Field()
    Beschreibung = Field(output_processor=remove_tags_from_list)
    Etage = Field()
    Kaufpreis = Field(output_processor=only_digit)
    Kaltmiete = Field()
    Warmmiete = Field()
    Nebenkosten = Field()
    Zimmeranzahl = Field()
    Wohnflaeche = Field()
    Monat = Field()
    url = Field()
    Telefon = Field()
    Erstellungsdatum = Field(output_processor=get_date)
    Gewerblich = Field(output_processor=choose_comm)