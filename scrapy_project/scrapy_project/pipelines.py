# -*- coding: utf-8 -*-
from table_definition import QuokaModel, engine, create_table
from sqlalchemy.orm import sessionmaker
from items import QuokaItem


class DetailPagePipeline(object):
    def __init__(self):
        self.Session = sessionmaker(bind=engine)
        create_table(engine)

    def open_spider(self, spider):
        self.session = self.Session()

    def close_spider(self, spider):
        self.session.close()

    def process_item(self, item, spider):
        if item.__class__ is QuokaItem:
            quoka_instance = QuokaModel(**item)
            self.session.add(quoka_instance)
            self.session.commit()
            return item