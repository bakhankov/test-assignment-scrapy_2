# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, MetaData, Column, Integer, Text, Date, String, BigInteger
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('sqlite:///quoka_base.db')
meta = MetaData()
DeclarativeBase = declarative_base()


def create_table(eng):
    DeclarativeBase.metadata.create_all(eng)


class QuokaModel(DeclarativeBase):
    __tablename__ = "quoka"
    id = Column('id', Integer, primary_key=True)
    Boersen_ID = Column('Boersen_ID', Integer)
    OBID = Column('OBID', Integer)
    erzeugt_am = Column('erzeugt_am', Date)
    Anbieter_ID = Column('Anbieter_ID', String(20))
    Anbieter_ObjektID = Column('Anbieter_ObjektID', String(100))
    Immobilientyp = Column('Immobilientyp', String(50))
    Immobilientyp_detail = Column('Immobilientyp_detail', String(200))
    Vermarktungstyp = Column('Vermarktungstyp', String(50))
    Land = Column('Land', String(30))
    Bundesland = Column('Bundesland', String(50))
    Bezirk = Column('Bezirk', String(150))
    Stadt = Column('Stadt', String(150))
    PLZ = Column('PLZ', String(10))
    Strasse = Column('Strasse', String(100))
    Hausnummer = Column('Hausnummer', String(40))
    Uberschrift = Column('Uberschrift', Text)
    Beschreibung = Column('Beschreibung', Text)
    Etage = Column('Etage', Integer)
    Kaufpreis = Column('Kaufpreis', BigInteger)
    Kaltmiete = Column('Kaltmiete', Integer)
    Warmmiete = Column('Warmmiete', Integer)
    Nebenkosten = Column('Nebenkosten', Integer)
    Zimmeranzahl = Column('Zimmeranzahl', Integer)
    Wohnflaeche = Column('Wohnflaeche', Integer)
    Monat = Column('Monat', Integer)
    url = Column('url', Text)
    Telefon = Column('Telefon', String(100))
    Erstellungsdatum = Column('Erstellungsdatum', Date)
    Gewerblich = Column('Gewerblich', Integer)